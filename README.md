# USUARIOS-API #

Api de creacion y consulta de usuarios.

### Tecnologias usadas ###
* Java 1.8
* Spring Boot 2.6.3
* HSQLDB (In memory)
* Flyway
* Junit5

### Requisitos previos ###

* Se debe tener instalado Java 1.8

### Clonar el proyecto ###

```bash
git clone https://avalerot@bitbucket.org/avalerot/usuarios-api.git
```

### Instalacion Linux ###

* Ir a la carpeta usuarios-api
```bash
cd usuarios-api
```
* Ejecutar el siguiente comando. Instala las dependencias necesarias y ejecuta la aplicacion.
```bash
cd ./gradlew bootRun
```

### Instalacion Windows ###

* Ir a la carpeta usuarios-api
```bash
cd usuarios-api
```
* Ejecutar el siguiente comando. Instala las dependencias necesarias y ejecuta la aplicacion.
```bash
cd gradlew.bat bootRun
```

### Scripts Base de datos ###
* Los script se encuentran src/main/resources/db/migration
* Se ejecutan de forma automatica al iniciar la aplicacion, por lo que no es necesario realizar ninguna accion.

### Listar usuarios ###

```bash
[GET] http://localhost:8080/usuarios/
```

* Lista todos los usuarios guardados

### Guardar un usuario ###

```bash
[POST] http://localhost:8080/usuarios/guardar
```

* Valida que la contraseña tenga al menos una letra mayuscula, una letra minuscula y 2 numeros
* Valida el formato del email ingresado
* Valida que el correo no exista en otro usuario
* Guarda un usuario
* Guarda los telefonos asociados al usuario

### Ejemplo peticion guardar usuario ###

![](/peticion_guardar.png)