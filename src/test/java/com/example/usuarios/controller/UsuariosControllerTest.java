package com.example.usuarios.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.usuarios.repository.UsuariosCRUDRepository;
import com.example.usuarios.repository.entity.Usuario;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UsuariosControllerTest {

  @MockBean
  private UsuariosCRUDRepository usuariosCRUDRepository;

  @Autowired
  private MockMvc mockMvc;

  @DisplayName("obtener lista usuarios exitoso")
  @Test
  void obtenerListaUsuariosExitoso() throws Exception {
    Usuario usuario1 = new Usuario();
    usuario1.setId(UUID.randomUUID().toString());
    usuario1.setName("Alvaro");
    usuario1.setEmail("test@test.cl");
    usuario1.setPassword("1234");
    Usuario usuario2 = new Usuario();
    usuario1.setId(UUID.randomUUID().toString());
    usuario1.setName("Pablo");
    usuario1.setEmail("pablo@test.cl");
    usuario1.setPassword("1234");
    List<Usuario> usuarios = Arrays.asList(usuario1, usuario2);
    Mockito.when(usuariosCRUDRepository.findAll())
            .thenReturn(usuarios);
    mockMvc.perform(get("http://localhost:8080/usuarios/"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", Matchers.hasSize(2)));
  }

  @DisplayName("obtener usuarios retorna error 500")
  @Test
  void retornaError500() throws Exception {
    Mockito.when(usuariosCRUDRepository.findAll())
        .thenReturn(null);
    mockMvc.perform(get("/usuarios/"))
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.mensaje", Matchers.is("Error interno del servidor")))
        .andExpect(jsonPath("$.codigo", Matchers.is(500)))
        .andDo(print());
  }

  @DisplayName("obtener usuarios retorna error 405")
  @Test
  void retornaMethodNotAllowed() throws Exception {
    mockMvc.perform(post("/usuarios/"))
        .andExpect(status().isMethodNotAllowed())
        .andExpect(jsonPath("$.mensaje", Matchers.is("Metodo no soportado")))
        .andExpect(jsonPath("$.codigo", Matchers.is(405)))
        .andDo(print());
  }
}