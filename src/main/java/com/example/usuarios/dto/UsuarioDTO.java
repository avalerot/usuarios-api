package com.example.usuarios.dto;

import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDTO {

  private String id;
  private String name;
  @NotNull(message = "El email no puede estar vacio")
  @NotEmpty(message = "El email no puede estar vacio")
  @Email(message = "El email no es valido", regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
      + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$")
  private String email;

  @NotNull(message = "La password no puede estar vacia")
  @NotEmpty(message = "La password no puede estar vacia")
  @Pattern(message = "La password debe tener al menos una mayuscula, una minuscula y 2 digitos",
      regexp = "^(?=.*?[0-9].*?[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z0-9]{4,}$")
  private String password;
  private LocalDateTime created;
  private LocalDateTime modified;
  private LocalDateTime lastLogin;
  private String token;
  private Boolean isActive;
  private List<TelefonoDTO> phones;
}
