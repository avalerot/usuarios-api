package com.example.usuarios.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefonoDTO {

  private Integer id;
  private String number;
  private Integer citycode;
  private Integer countrycode;
  private String idusuario;

}
