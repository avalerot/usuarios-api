package com.example.usuarios.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ErrorDTO {

  private String mensaje;
  private Integer codigo;
}
