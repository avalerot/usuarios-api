package com.example.usuarios.service.builder;

import com.example.usuarios.dto.TelefonoDTO;
import com.example.usuarios.repository.entity.Telefono;
import org.springframework.stereotype.Component;

@Component
public class TelefonoDTOBulder {

  private Telefono telefono;

  public TelefonoDTOBulder fromTelefono(Telefono telefono) {
    this.telefono = telefono;
    return this;
  }

  public TelefonoDTO build() {
    if (this.telefono == null) {
      return null;
    }
    TelefonoDTO telefonoDTO = new TelefonoDTO();
    telefonoDTO.setCitycode(this.telefono.getCitycode());
    telefonoDTO.setCountrycode(this.telefono.getCountrycode());
    telefonoDTO.setNumber(this.telefono.getNumber());
    return telefonoDTO;
  }
}

