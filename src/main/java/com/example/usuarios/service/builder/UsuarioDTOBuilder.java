package com.example.usuarios.service.builder;

import com.example.usuarios.dto.UsuarioDTO;
import com.example.usuarios.repository.entity.Usuario;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsuarioDTOBuilder {

  private final TelefonoDTOBulder telefonoDTOBulder;

  private Usuario usuario;

  @Autowired
  public UsuarioDTOBuilder(TelefonoDTOBulder telefonoDTOBulder) {
    this.telefonoDTOBulder = telefonoDTOBulder;
  }

  public UsuarioDTOBuilder fromUsuario(Usuario usuario) {
    this.usuario = usuario;
    return this;
  }

  public UsuarioDTO build() {
    if (this.usuario == null) {
      return null;
    }
    UsuarioDTO usuarioDTO = new UsuarioDTO();
    usuarioDTO.setId(this.usuario.getId());
    usuarioDTO.setIsActive(this.usuario.getIsActive());
    usuarioDTO.setCreated(this.usuario.getCreated());
    usuarioDTO.setEmail(this.usuario.getEmail());
    usuarioDTO.setModified(this.usuario.getModified());
    usuarioDTO.setLastLogin(this.usuario.getLastLogin());
    usuarioDTO.setName(this.usuario.getName());
    usuarioDTO.setPassword(this.usuario.getPassword());
    usuarioDTO.setToken(this.usuario.getToken());
    if (this.usuario.getPhones() != null) {
      usuarioDTO.setPhones(this.usuario.getPhones()
          .stream().map(t -> this.telefonoDTOBulder.fromTelefono(t).build())
          .collect(Collectors.toList()));
    }
    return usuarioDTO;
  }
}
