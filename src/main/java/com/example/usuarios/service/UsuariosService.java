package com.example.usuarios.service;

import com.example.usuarios.dto.UsuarioDTO;
import java.util.List;

public interface UsuariosService {

  UsuarioDTO save(UsuarioDTO usuarioDTO);
  List<UsuarioDTO> listAll();
}
