package com.example.usuarios.service.impl;

import com.example.usuarios.configuration.ExcepcionCorreo;
import com.example.usuarios.dto.UsuarioDTO;
import com.example.usuarios.repository.TelefonosCRUDRepository;
import com.example.usuarios.repository.UsuariosCRUDRepository;
import com.example.usuarios.repository.UsuariosRepository;
import com.example.usuarios.repository.builder.TelefonoBuilder;
import com.example.usuarios.repository.builder.UsuarioBuilder;
import com.example.usuarios.repository.entity.Telefono;
import com.example.usuarios.repository.entity.Usuario;
import com.example.usuarios.service.UsuariosService;
import com.example.usuarios.service.builder.UsuarioDTOBuilder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuariosServiceImpl implements UsuariosService {

  private static final Log LOGGER = LogFactory.getLog(UsuariosService.class);
  private final UsuariosRepository usuariosRepository;
  private final UsuariosCRUDRepository usuariosCRUDRepository;
  private final UsuarioDTOBuilder usuarioDTOBuilder;
  private final UsuarioBuilder usuarioBuilder;
  private final TelefonosCRUDRepository telefonosCRUDRepository;
  private final TelefonoBuilder telefonoBuilder;

  @Autowired
  public UsuariosServiceImpl(UsuariosRepository usuariosRepository,
      UsuariosCRUDRepository usuariosCRUDRepository,
      UsuarioDTOBuilder usuarioDTOBuilder,
      UsuarioBuilder usuarioBuilder,
      TelefonosCRUDRepository telefonosCRUDRepository,
      TelefonoBuilder telefonoBuilder) {
    this.usuariosRepository = usuariosRepository;
    this.usuariosCRUDRepository = usuariosCRUDRepository;
    this.usuarioDTOBuilder = usuarioDTOBuilder;
    this.usuarioBuilder = usuarioBuilder;
    this.telefonosCRUDRepository = telefonosCRUDRepository;
    this.telefonoBuilder = telefonoBuilder;
  }

  @Override
  @Transactional
  public UsuarioDTO save(UsuarioDTO usuarioDTO) {
    LOGGER.debug("**********Servicio guardar usuarios**********");
    if (this.usuariosRepository.existeEmail(usuarioDTO.getEmail()).isPresent()) {
      throw new ExcepcionCorreo();
    }
    Usuario usuario = usuarioBuilder.fromUsuarioDTO(usuarioDTO).build();
    usuario.setId(UUID.randomUUID().toString());
    usuario.setToken(UUID.randomUUID().toString());
    this.usuariosCRUDRepository.save(usuario);
    this.verificarTelefonosGuardar(usuarioDTO, usuario);
    return usuarioDTOBuilder.fromUsuario(usuario).build();
  }

  @Override
  public List<UsuarioDTO> listAll() {
    LOGGER.debug("**********Servicio listar usuarios**********");
    List<UsuarioDTO> usuarios = new ArrayList<>();
    this.usuariosCRUDRepository.findAll().iterator().forEachRemaining(
        usuario -> usuarios.add(this.usuarioDTOBuilder.fromUsuario(usuario).build()));
    return usuarios;
  }

  @Transactional
  public void verificarTelefonosGuardar(UsuarioDTO usuarioDTO, Usuario usuario) {
    LOGGER.debug("**********Servicio verificarTelefonosGuardar usuarios**********");
    if (usuarioDTO.getPhones() != null && !usuarioDTO.getPhones().isEmpty()) {
      List<Telefono> telefonos =
          usuarioDTO.getPhones().stream()
            .peek(telefonoDTO -> telefonoDTO.setIdusuario(usuario.getId()))
            .map(telefonoDTO -> this.telefonoBuilder.fromTelefonoDTO(telefonoDTO).build())
            .collect(Collectors.toList());
      this.telefonosCRUDRepository.saveAll(telefonos);
    }
  }
}
