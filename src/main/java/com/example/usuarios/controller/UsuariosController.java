package com.example.usuarios.controller;

import com.example.usuarios.dto.UsuarioDTO;
import com.example.usuarios.service.UsuariosService;
import java.util.List;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@Validated
public class UsuariosController {

  private static final Log LOGGER = LogFactory.getLog(UsuariosController.class);
  private final UsuariosService usuariosService;

  @Autowired
  public UsuariosController(UsuariosService usuariosService) {
    this.usuariosService = usuariosService;
  }

  @ResponseBody
  @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<UsuarioDTO> findAll() {
    LOGGER.debug("**********Nueva peticion listar usuarios**********");
    return this.usuariosService.listAll();
  }

  @ResponseBody
  @PostMapping(value = "/guardar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public UsuarioDTO save(@RequestBody @Valid UsuarioDTO usuarioDTO) {
    LOGGER.debug("**********Nueva peticion guardar usuarios**********");
    return this.usuariosService.save(usuarioDTO);
  }
}
