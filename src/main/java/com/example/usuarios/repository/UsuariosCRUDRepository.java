package com.example.usuarios.repository;

import com.example.usuarios.repository.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosCRUDRepository extends CrudRepository<Usuario, String> {

}
