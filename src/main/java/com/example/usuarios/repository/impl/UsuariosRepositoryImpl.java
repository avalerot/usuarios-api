package com.example.usuarios.repository.impl;

import com.example.usuarios.repository.UsuariosRepository;
import com.example.usuarios.repository.entity.Usuario;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsuariosRepositoryImpl implements UsuariosRepository {

  private final EntityManager em;

  @Autowired
  public UsuariosRepositoryImpl(EntityManager em) {
    this.em = em;
  }

  @Override
  public Optional<Usuario> existeEmail(String email) {
    return em.createQuery("select u from Usuario u "
        + "where u.email = :email", Usuario.class)
        .setParameter("email", email)
        .getResultList().stream().findFirst();
  }
}
