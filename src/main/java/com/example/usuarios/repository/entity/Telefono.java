package com.example.usuarios.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;


@Table(name = "telefonos")
@Getter
@Setter
@Entity
public class Telefono {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Integer id;

  @Column(name = "number")
  private String number;

  @Column(name = "citycode")
  private Integer citycode;

  @Column(name = "countrycode")
  private Integer countrycode;

  @Column(name = "idusuario")
  private String idusuario;
}
