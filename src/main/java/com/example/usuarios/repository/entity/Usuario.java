package com.example.usuarios.repository.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Table(name = "usuarios")
@Getter
@Setter
@Entity
public class Usuario {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "name")
  private String name;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @CreationTimestamp
  @Column(name = "created")
  private LocalDateTime created;

  @UpdateTimestamp
  @Column(name = "modified")
  private LocalDateTime modified;

  @CreationTimestamp
  @Column(name = "last_login")
  private LocalDateTime lastLogin;

  @Column(name = "token")
  private String token;

  @Column(name = "is_active")
  private Boolean isActive;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "idusuario")
  private List<Telefono> phones;
}
