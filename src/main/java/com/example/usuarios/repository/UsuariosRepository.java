package com.example.usuarios.repository;


import com.example.usuarios.repository.entity.Usuario;
import java.util.Optional;

public interface UsuariosRepository {

  Optional<Usuario> existeEmail(String email);
}
