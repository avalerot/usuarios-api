package com.example.usuarios.repository;

import com.example.usuarios.repository.entity.Telefono;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TelefonosCRUDRepository extends CrudRepository<Telefono, Integer> {

}
