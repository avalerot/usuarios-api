package com.example.usuarios.repository.builder;

import com.example.usuarios.dto.UsuarioDTO;
import com.example.usuarios.repository.entity.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UsuarioBuilder {

  private UsuarioDTO usuarioDTO;

  public UsuarioBuilder fromUsuarioDTO(UsuarioDTO usuarioDTO) {
    this.usuarioDTO = usuarioDTO;
    return this;
  }

  public Usuario build() {
    if (this.usuarioDTO == null) {
      return null;
    }
    Usuario usuario = new Usuario();
    usuario.setId(this.usuarioDTO.getId());
    usuario.setIsActive(this.usuarioDTO.getIsActive());
    usuario.setCreated(this.usuarioDTO.getCreated());
    usuario.setEmail(this.usuarioDTO.getEmail());
    usuario.setModified(this.usuarioDTO.getModified());
    usuario.setLastLogin(this.usuarioDTO.getLastLogin());
    usuario.setName(this.usuarioDTO.getName());
    usuario.setPassword(this.usuarioDTO.getPassword());
    usuario.setToken(this.usuarioDTO.getToken());
    return usuario;
  }
}
