package com.example.usuarios.repository.builder;

import com.example.usuarios.dto.TelefonoDTO;
import com.example.usuarios.repository.entity.Telefono;
import org.springframework.stereotype.Component;

@Component
public class TelefonoBuilder {

  private TelefonoDTO telefonoDTO;

  public TelefonoBuilder fromTelefonoDTO(TelefonoDTO telefonoDTO) {
    this.telefonoDTO = telefonoDTO;
    return this;
  }

  public Telefono build() {
    if (this.telefonoDTO == null) {
      return null;
    }
    Telefono telefono = new Telefono();
    telefono.setCitycode(this.telefonoDTO.getCitycode());
    telefono.setCountrycode(this.telefonoDTO.getCountrycode());
    telefono.setId(this.telefonoDTO.getId());
    telefono.setNumber(this.telefonoDTO.getNumber());
    telefono.setIdusuario(this.telefonoDTO.getIdusuario());
    return telefono;
  }
}
