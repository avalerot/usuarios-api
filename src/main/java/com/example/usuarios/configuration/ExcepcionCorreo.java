package com.example.usuarios.configuration;

import lombok.Getter;

@Getter
public class ExcepcionCorreo extends RuntimeException{

  private final String mensaje = "El correo ya existe";
}
