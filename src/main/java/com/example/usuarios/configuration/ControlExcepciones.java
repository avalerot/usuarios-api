package com.example.usuarios.configuration;

import com.example.usuarios.dto.ErrorDTO;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControlExcepciones {

  private static final Log LOG = LogFactory.getLog(ControlExcepciones.class);

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<ErrorDTO> httpRequestMethodNotSupportedException(
      HttpRequestMethodNotSupportedException e) {
    LOG.error(e.getMessage(), e.getCause());
    return new ResponseEntity<>(
        ErrorDTO.builder()
            .mensaje("Metodo no soportado")
            .codigo(HttpStatus.METHOD_NOT_ALLOWED.value())
            .build()
        , HttpStatus.METHOD_NOT_ALLOWED);
  }

  @ExceptionHandler({NullPointerException.class, Exception.class})
  public ResponseEntity<ErrorDTO> errorInternoDelServidor(Exception e) {
    LOG.error(e.getMessage(), e.getCause());
    e.printStackTrace();
    return new ResponseEntity<>(
        ErrorDTO.builder()
            .mensaje("Error interno del servidor")
            .codigo(HttpStatus.INTERNAL_SERVER_ERROR.value())
            .build()
        , HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ExcepcionCorreo.class)
  public ResponseEntity<ErrorDTO> errorCorreoYaExiste(ExcepcionCorreo e) {
    LOG.error(e.getMessage());
    return new ResponseEntity<>(
        ErrorDTO.builder()
            .mensaje(e.getMensaje())
            .codigo(HttpStatus.CONFLICT.value())
            .build()
        , HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ErrorDTO> constraintViolationException(ConstraintViolationException e) {
    LOG.error(e.getMessage(), e.getCause());
    Set<ConstraintViolation<?>> result = e.getConstraintViolations();
    StringBuilder errorMessage = new StringBuilder();
    result.forEach(f -> errorMessage.append(f.getMessage()).append(" "));
    return new ResponseEntity<>(
    ErrorDTO.builder()
        .mensaje(errorMessage.toString())
        .codigo(HttpStatus.BAD_REQUEST.value())
        .build()
        , HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorDTO> methodArgumentNotValidException(MethodArgumentNotValidException e) {
    LOG.error(e.getMessage(), e.getCause());
    BindingResult result = e.getBindingResult();
    List<FieldError> fieldErrors = result.getFieldErrors();
    StringBuilder errorMessage = new StringBuilder();
    fieldErrors.forEach(f -> errorMessage.append(f.getDefaultMessage()).append(". "));
    return new ResponseEntity<>(
    ErrorDTO.builder()
        .mensaje(errorMessage.toString())
        .codigo(HttpStatus.BAD_REQUEST.value())
        .build()
        , HttpStatus.BAD_REQUEST);
  }
}
