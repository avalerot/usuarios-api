CREATE TABLE ${schema_name}.usuarios (
    id VARCHAR(36)  NOT NULL,
    name VARCHAR (100),
    email VARCHAR (100),
    password VARCHAR(255),
    is_active boolean,
    token varchar(255),
    last_login datetime,
    created datetime,
    modified datetime,
    PRIMARY KEY (ID)
);