CREATE TABLE ${schema_name}.telefonos (
                                         id INT NOT NULL,
                                         number varchar(12),
                                         citycode int,
                                         countrycode int,
                                         idusuario varchar(36) NOT NULL,
                                         PRIMARY KEY (ID)
);

ALTER TABLE ${schema_name}.telefonos ADD FOREIGN KEY (idusuario) REFERENCES ${schema_name}.usuarios(id);